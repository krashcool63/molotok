var menu = (function(module){

	module.menuWrap = document.getElementById('headMenu');
	module.menuBtn = document.getElementById('headMobileBtn');

	module.init = function(){
		module.menuBtn.addEventListener('click', function(){
			module.menuWrap.classList.toggle('head__menu--open');
		})
	}

	module.init();

	return module;
})(menu || {});
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJjb21tb24uanMiXSwic291cmNlc0NvbnRlbnQiOlsidmFyIG1lbnUgPSAoZnVuY3Rpb24obW9kdWxlKXtcclxuXHJcblx0bW9kdWxlLm1lbnVXcmFwID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ2hlYWRNZW51Jyk7XHJcblx0bW9kdWxlLm1lbnVCdG4gPSBkb2N1bWVudC5nZXRFbGVtZW50QnlJZCgnaGVhZE1vYmlsZUJ0bicpO1xyXG5cclxuXHRtb2R1bGUuaW5pdCA9IGZ1bmN0aW9uKCl7XHJcblx0XHRtb2R1bGUubWVudUJ0bi5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZ1bmN0aW9uKCl7XHJcblx0XHRcdG1vZHVsZS5tZW51V3JhcC5jbGFzc0xpc3QudG9nZ2xlKCdoZWFkX19tZW51LS1vcGVuJyk7XHJcblx0XHR9KVxyXG5cdH1cclxuXHJcblx0bW9kdWxlLmluaXQoKTtcclxuXHJcblx0cmV0dXJuIG1vZHVsZTtcclxufSkobWVudSB8fCB7fSk7Il0sImZpbGUiOiJjb21tb24uanMiLCJzb3VyY2VSb290IjoiL3NvdXJjZS8ifQ==
