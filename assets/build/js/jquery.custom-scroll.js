/* jQuery Custom Scroll plugin v0.6.6 | (c) 2015 Mostovoy Andrey | https://github.com/standy/custom-scroll/blob/master/LICENSE */
(function($) {
	$.fn.customScroll = function(options) {
		if (!this.length) {
			return $.extend(defaultOptions, options);
		}
		if (options==='destroy') {
			this.each(function() {
				var cs = $(this).data('custom-scroll');
				if (cs) cs.destroy();
			});
			return this;
		}
		if (this.length===1) return customScroll(this, options);
		this.each(function() {
			customScroll($(this), options);
		});
	};


	var defaultOptions = {
		prefix: 'custom-scroll_',
		barMinHeight: 10,
		barMinWidth:  10,
		offsetTop:    0,
		offsetBottom: 0,
		offsetLeft:   0,
		offsetRight:  0,
		trackWidth:   10,
		trackHeight:  10,
		barHtml: '<div />',
		vertical: true,
		horizontal: false,
		preventParentScroll: true
	};

	var DIRS_VERTICAL = {
//		axis: 'y',
		dim: 'height',
		Dim: 'Height',
		dir: 'top',
		Dir: 'Top',
		dir2: 'bottom',
		Dir2: 'Bottom',
		clientAxis: 'clientY',
		suffix: '-y'
	};
	var DIRS_HORIZONTAL = {
//		axis: 'x',
		dim: 'width',
		Dim: 'Width',
		dir: 'left',
		Dir: 'Left',
		dir2: 'right',
		Dir2: 'Right',
		clientAxis: 'clientX',
		suffix: '-x'
	};

	function customScroll($container, options) {
		var cs = $container.data('custom-scroll');
		if (cs) options = cs.options;
		else options = $.extend({}, defaultOptions, options);
		var dirs = {};
		var lastDims = {};

		var isBarHidden = {
			x: +options.vertical,
			y: +options.horizontal
		};

		if (options.horizontal) {
			dirs.x = DIRS_HORIZONTAL;
			lastDims.x = {};
		}
		if (options.vertical) {
			dirs.y = DIRS_VERTICAL;
			lastDims.y = {};
		}

		if ($container.hasClass(options.prefix+'container') && cs) {
			cs.updateBars();
			return cs;
		}
		var $inner = $container.children('.'+options.prefix+'inner');
		if (!$inner.length) {
			$inner = $container.wrapInner('<div class="'+options.prefix+'inner'+'"></div>').children();
		}

		$container.addClass(options.prefix+'container');


		// scroll dimensions in case of hidden element
		var tmp = $('<div class="'+ options.prefix+'inner" />').width(100).height(100).appendTo('body').css({overflow:'scroll'})[0];
		var scrollWidth = tmp.offsetWidth-tmp.clientWidth;
		var scrollHeight = tmp.offsetHeight-tmp.clientHeight;
		tmp.parentElement.removeChild(tmp);

		if (options.vertical) {
			$inner.css({
				/* save the padding */
				paddingLeft: $container.css('paddingLeft'),
				paddingRight: $container.css('paddingRight'),
				/* hide scrolls */
				marginRight: -scrollWidth+'px'

			});
			$container.css({
				paddingLeft: 0,
				paddingRight: 0
			});
		} else {
			$inner.css({overflowY: 'hidden'})
		}
		if (options.horizontal) {
			$inner.css({
				/* hide scrolls */
				marginBottom: -scrollHeight+'px',
				paddingBottom: scrollHeight+'px'
			});
			$container.css({
				paddingTop: 0,
				paddingBottom: 0
			});
		} else {
			$inner.css({overflowX: 'hidden'})
		}

		/* in case of max-height */
		var maxHeight = $container.css('maxHeight');
		if (parseInt(maxHeight)) {
			$container.css('maxHeight', 'none');
			$inner.css('maxHeight', maxHeight);
		}


		$container.scrollTop(0);


		var $body = $('body');

		var $bars = {};
		$.each(dirs, initBar);

		$inner.on('scroll', updateBars);
		updateBars();
		if (options.preventParentScroll) preventParentScroll();

		var data = {
			$container: $container,
			$bar: $bars.y,
			$barX: $bars.x,
			$inner: $inner,
			destroy: destroy,
			updateBars: updateBars,
			options: options
		};
		$container.data('custom-scroll', data);
		return data;


		function preventParentScroll() {
			$inner.on('DOMMouseScroll mousewheel', function(e) {
				var $this = $(this);
				var scrollTop = this.scrollTop;
				var scrollHeight = this.scrollHeight;
				var height = $this.height();
				var delta = (e.type == 'DOMMouseScroll' ? e.originalEvent.detail * -40 : e.originalEvent.wheelDelta);
				var up = delta > 0;

				if (up ? scrollTop === 0 : scrollTop === scrollHeight - height) {
					e.preventDefault();
				}
			});
		}

		function initBar(dirKey, dir) {
//			console.log('initBar', dirKey, dir)
//			var dir = DIRS[dirKey];
			$container['scroll' + dir.Dir](0);

			var cls = options.prefix+'bar'+dir.suffix;
			var $bar = $container.children('.'+ cls);
			if (!$bar.length) {
				$bar = $(options.barHtml).addClass(cls).appendTo($container);
			}

			$bar.on('mousedown touchstart', function(e) {
				e.preventDefault(); // stop scrolling in ie9
				var scrollStart = $inner['scroll' + dir.Dir]();
				var posStart = e[dir.clientAxis] || e.originalEvent.changedTouches && e.originalEvent.changedTouches[0][dir.clientAxis];
				var ratio = getDims(dirKey, dir).ratio;

				$body.on('mousemove.custom-scroll touchmove.custom-scroll', function(e) {
					e.preventDefault(); // stop scrolling
					var pos = e[dir.clientAxis] || e.originalEvent.changedTouches && e.originalEvent.changedTouches[0][dir.clientAxis];
					$inner['scroll' + dir.Dir](scrollStart + (pos-posStart)/ratio);
				});
				$body.on('mouseup.custom-scroll touchend.custom-scroll', function() {
					$body.off('.custom-scroll');
				});
			});
			$bars[dirKey] = $bar;
		}

		function getDims(dirKey, dir) {
//			console.log('getDims', dirKey, dir)
			var total = $inner.prop('scroll' + dir.Dim)|0;
			var dim = $container['inner' + dir.Dim]();
			var inner = $inner['inner' + dir.Dim]();
			var scroll = dim - options['offset' + dir.Dir] - options['offset' + dir.Dir2];
			if (!isBarHidden[dirKey == 'x' ? 'y' : 'x']) scroll -= options['track'+dir.Dim];

			var bar = Math.max((scroll*dim/total)|0, options['barMin' + dir.Dim]);
			var ratio = (scroll-bar)/(total-inner);
//			if (dirKey == 'y' && $container.is('#example-hard')) console.log('dim', dim, inner, scroll, total, bar, ratio)

			return {
				ratio: ratio,
				dim: dim,
				scroll: scroll,
				total: total,
				bar: bar
			}
		}

		function updateBars() {
			$.each(dirs, updateBar);
		}
		function updateBar(dirKey, dir) {
//			var dir = DIRS[dirKey];
			var dims = getDims(dirKey, dir);
			if (!dims.total) return;

			var scrollPos = $inner['scroll' + dir.Dir]();
			if (
				lastDims[dirKey].scrollPos === scrollPos &&
				lastDims[dirKey].scroll === dims.scroll &&
				lastDims[dirKey].total === dims.total
			) return;
			lastDims[dirKey] = dims;
			lastDims[dirKey].scrollPos = scrollPos;


			var isHide = dims.bar>=dims.scroll;
			if (isHide!==isBarHidden[dirKey]) {
				$container.toggleClass(options.prefix+'hidden'+dir.suffix, isHide);
				isBarHidden[dirKey] = isHide;
			}
			var barPos = scrollPos*dims.ratio;
//			console.log('upd', scrollPos, dims.ratio, barPos)
			//if (dirKey === 'y') console.log(barPos, dims.scroll, dims.bar, dims)
			if (barPos<0) barPos = 0;
			if (barPos>dims.scroll-dims.bar) barPos = dims.scroll-dims.bar;
			$bars[dirKey][dir.dim](dims.bar)
				.css(dir.dir, options['offset' + dir.Dir] + barPos);
		}

		function destroy() {
			$.each(dirs, function(key) { $bars[key].remove(); });
			$container
				.removeClass(options.prefix+'container')
				.removeData('custom-scroll')
				.css({padding: '', maxHeight: ''});
			$inner.contents().appendTo($container);
			$inner.remove();
		}
	}
})(jQuery);
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJqcXVlcnkuY3VzdG9tLXNjcm9sbC5qcyJdLCJzb3VyY2VzQ29udGVudCI6WyIvKiBqUXVlcnkgQ3VzdG9tIFNjcm9sbCBwbHVnaW4gdjAuNi42IHwgKGMpIDIwMTUgTW9zdG92b3kgQW5kcmV5IHwgaHR0cHM6Ly9naXRodWIuY29tL3N0YW5keS9jdXN0b20tc2Nyb2xsL2Jsb2IvbWFzdGVyL0xJQ0VOU0UgKi9cclxuKGZ1bmN0aW9uKCQpIHtcclxuXHQkLmZuLmN1c3RvbVNjcm9sbCA9IGZ1bmN0aW9uKG9wdGlvbnMpIHtcclxuXHRcdGlmICghdGhpcy5sZW5ndGgpIHtcclxuXHRcdFx0cmV0dXJuICQuZXh0ZW5kKGRlZmF1bHRPcHRpb25zLCBvcHRpb25zKTtcclxuXHRcdH1cclxuXHRcdGlmIChvcHRpb25zPT09J2Rlc3Ryb3knKSB7XHJcblx0XHRcdHRoaXMuZWFjaChmdW5jdGlvbigpIHtcclxuXHRcdFx0XHR2YXIgY3MgPSAkKHRoaXMpLmRhdGEoJ2N1c3RvbS1zY3JvbGwnKTtcclxuXHRcdFx0XHRpZiAoY3MpIGNzLmRlc3Ryb3koKTtcclxuXHRcdFx0fSk7XHJcblx0XHRcdHJldHVybiB0aGlzO1xyXG5cdFx0fVxyXG5cdFx0aWYgKHRoaXMubGVuZ3RoPT09MSkgcmV0dXJuIGN1c3RvbVNjcm9sbCh0aGlzLCBvcHRpb25zKTtcclxuXHRcdHRoaXMuZWFjaChmdW5jdGlvbigpIHtcclxuXHRcdFx0Y3VzdG9tU2Nyb2xsKCQodGhpcyksIG9wdGlvbnMpO1xyXG5cdFx0fSk7XHJcblx0fTtcclxuXHJcblxyXG5cdHZhciBkZWZhdWx0T3B0aW9ucyA9IHtcclxuXHRcdHByZWZpeDogJ2N1c3RvbS1zY3JvbGxfJyxcclxuXHRcdGJhck1pbkhlaWdodDogMTAsXHJcblx0XHRiYXJNaW5XaWR0aDogIDEwLFxyXG5cdFx0b2Zmc2V0VG9wOiAgICAwLFxyXG5cdFx0b2Zmc2V0Qm90dG9tOiAwLFxyXG5cdFx0b2Zmc2V0TGVmdDogICAwLFxyXG5cdFx0b2Zmc2V0UmlnaHQ6ICAwLFxyXG5cdFx0dHJhY2tXaWR0aDogICAxMCxcclxuXHRcdHRyYWNrSGVpZ2h0OiAgMTAsXHJcblx0XHRiYXJIdG1sOiAnPGRpdiAvPicsXHJcblx0XHR2ZXJ0aWNhbDogdHJ1ZSxcclxuXHRcdGhvcml6b250YWw6IGZhbHNlLFxyXG5cdFx0cHJldmVudFBhcmVudFNjcm9sbDogdHJ1ZVxyXG5cdH07XHJcblxyXG5cdHZhciBESVJTX1ZFUlRJQ0FMID0ge1xyXG4vL1x0XHRheGlzOiAneScsXHJcblx0XHRkaW06ICdoZWlnaHQnLFxyXG5cdFx0RGltOiAnSGVpZ2h0JyxcclxuXHRcdGRpcjogJ3RvcCcsXHJcblx0XHREaXI6ICdUb3AnLFxyXG5cdFx0ZGlyMjogJ2JvdHRvbScsXHJcblx0XHREaXIyOiAnQm90dG9tJyxcclxuXHRcdGNsaWVudEF4aXM6ICdjbGllbnRZJyxcclxuXHRcdHN1ZmZpeDogJy15J1xyXG5cdH07XHJcblx0dmFyIERJUlNfSE9SSVpPTlRBTCA9IHtcclxuLy9cdFx0YXhpczogJ3gnLFxyXG5cdFx0ZGltOiAnd2lkdGgnLFxyXG5cdFx0RGltOiAnV2lkdGgnLFxyXG5cdFx0ZGlyOiAnbGVmdCcsXHJcblx0XHREaXI6ICdMZWZ0JyxcclxuXHRcdGRpcjI6ICdyaWdodCcsXHJcblx0XHREaXIyOiAnUmlnaHQnLFxyXG5cdFx0Y2xpZW50QXhpczogJ2NsaWVudFgnLFxyXG5cdFx0c3VmZml4OiAnLXgnXHJcblx0fTtcclxuXHJcblx0ZnVuY3Rpb24gY3VzdG9tU2Nyb2xsKCRjb250YWluZXIsIG9wdGlvbnMpIHtcclxuXHRcdHZhciBjcyA9ICRjb250YWluZXIuZGF0YSgnY3VzdG9tLXNjcm9sbCcpO1xyXG5cdFx0aWYgKGNzKSBvcHRpb25zID0gY3Mub3B0aW9ucztcclxuXHRcdGVsc2Ugb3B0aW9ucyA9ICQuZXh0ZW5kKHt9LCBkZWZhdWx0T3B0aW9ucywgb3B0aW9ucyk7XHJcblx0XHR2YXIgZGlycyA9IHt9O1xyXG5cdFx0dmFyIGxhc3REaW1zID0ge307XHJcblxyXG5cdFx0dmFyIGlzQmFySGlkZGVuID0ge1xyXG5cdFx0XHR4OiArb3B0aW9ucy52ZXJ0aWNhbCxcclxuXHRcdFx0eTogK29wdGlvbnMuaG9yaXpvbnRhbFxyXG5cdFx0fTtcclxuXHJcblx0XHRpZiAob3B0aW9ucy5ob3Jpem9udGFsKSB7XHJcblx0XHRcdGRpcnMueCA9IERJUlNfSE9SSVpPTlRBTDtcclxuXHRcdFx0bGFzdERpbXMueCA9IHt9O1xyXG5cdFx0fVxyXG5cdFx0aWYgKG9wdGlvbnMudmVydGljYWwpIHtcclxuXHRcdFx0ZGlycy55ID0gRElSU19WRVJUSUNBTDtcclxuXHRcdFx0bGFzdERpbXMueSA9IHt9O1xyXG5cdFx0fVxyXG5cclxuXHRcdGlmICgkY29udGFpbmVyLmhhc0NsYXNzKG9wdGlvbnMucHJlZml4Kydjb250YWluZXInKSAmJiBjcykge1xyXG5cdFx0XHRjcy51cGRhdGVCYXJzKCk7XHJcblx0XHRcdHJldHVybiBjcztcclxuXHRcdH1cclxuXHRcdHZhciAkaW5uZXIgPSAkY29udGFpbmVyLmNoaWxkcmVuKCcuJytvcHRpb25zLnByZWZpeCsnaW5uZXInKTtcclxuXHRcdGlmICghJGlubmVyLmxlbmd0aCkge1xyXG5cdFx0XHQkaW5uZXIgPSAkY29udGFpbmVyLndyYXBJbm5lcignPGRpdiBjbGFzcz1cIicrb3B0aW9ucy5wcmVmaXgrJ2lubmVyJysnXCI+PC9kaXY+JykuY2hpbGRyZW4oKTtcclxuXHRcdH1cclxuXHJcblx0XHQkY29udGFpbmVyLmFkZENsYXNzKG9wdGlvbnMucHJlZml4Kydjb250YWluZXInKTtcclxuXHJcblxyXG5cdFx0Ly8gc2Nyb2xsIGRpbWVuc2lvbnMgaW4gY2FzZSBvZiBoaWRkZW4gZWxlbWVudFxyXG5cdFx0dmFyIHRtcCA9ICQoJzxkaXYgY2xhc3M9XCInKyBvcHRpb25zLnByZWZpeCsnaW5uZXJcIiAvPicpLndpZHRoKDEwMCkuaGVpZ2h0KDEwMCkuYXBwZW5kVG8oJ2JvZHknKS5jc3Moe292ZXJmbG93OidzY3JvbGwnfSlbMF07XHJcblx0XHR2YXIgc2Nyb2xsV2lkdGggPSB0bXAub2Zmc2V0V2lkdGgtdG1wLmNsaWVudFdpZHRoO1xyXG5cdFx0dmFyIHNjcm9sbEhlaWdodCA9IHRtcC5vZmZzZXRIZWlnaHQtdG1wLmNsaWVudEhlaWdodDtcclxuXHRcdHRtcC5wYXJlbnRFbGVtZW50LnJlbW92ZUNoaWxkKHRtcCk7XHJcblxyXG5cdFx0aWYgKG9wdGlvbnMudmVydGljYWwpIHtcclxuXHRcdFx0JGlubmVyLmNzcyh7XHJcblx0XHRcdFx0Lyogc2F2ZSB0aGUgcGFkZGluZyAqL1xyXG5cdFx0XHRcdHBhZGRpbmdMZWZ0OiAkY29udGFpbmVyLmNzcygncGFkZGluZ0xlZnQnKSxcclxuXHRcdFx0XHRwYWRkaW5nUmlnaHQ6ICRjb250YWluZXIuY3NzKCdwYWRkaW5nUmlnaHQnKSxcclxuXHRcdFx0XHQvKiBoaWRlIHNjcm9sbHMgKi9cclxuXHRcdFx0XHRtYXJnaW5SaWdodDogLXNjcm9sbFdpZHRoKydweCdcclxuXHJcblx0XHRcdH0pO1xyXG5cdFx0XHQkY29udGFpbmVyLmNzcyh7XHJcblx0XHRcdFx0cGFkZGluZ0xlZnQ6IDAsXHJcblx0XHRcdFx0cGFkZGluZ1JpZ2h0OiAwXHJcblx0XHRcdH0pO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0JGlubmVyLmNzcyh7b3ZlcmZsb3dZOiAnaGlkZGVuJ30pXHJcblx0XHR9XHJcblx0XHRpZiAob3B0aW9ucy5ob3Jpem9udGFsKSB7XHJcblx0XHRcdCRpbm5lci5jc3Moe1xyXG5cdFx0XHRcdC8qIGhpZGUgc2Nyb2xscyAqL1xyXG5cdFx0XHRcdG1hcmdpbkJvdHRvbTogLXNjcm9sbEhlaWdodCsncHgnLFxyXG5cdFx0XHRcdHBhZGRpbmdCb3R0b206IHNjcm9sbEhlaWdodCsncHgnXHJcblx0XHRcdH0pO1xyXG5cdFx0XHQkY29udGFpbmVyLmNzcyh7XHJcblx0XHRcdFx0cGFkZGluZ1RvcDogMCxcclxuXHRcdFx0XHRwYWRkaW5nQm90dG9tOiAwXHJcblx0XHRcdH0pO1xyXG5cdFx0fSBlbHNlIHtcclxuXHRcdFx0JGlubmVyLmNzcyh7b3ZlcmZsb3dYOiAnaGlkZGVuJ30pXHJcblx0XHR9XHJcblxyXG5cdFx0LyogaW4gY2FzZSBvZiBtYXgtaGVpZ2h0ICovXHJcblx0XHR2YXIgbWF4SGVpZ2h0ID0gJGNvbnRhaW5lci5jc3MoJ21heEhlaWdodCcpO1xyXG5cdFx0aWYgKHBhcnNlSW50KG1heEhlaWdodCkpIHtcclxuXHRcdFx0JGNvbnRhaW5lci5jc3MoJ21heEhlaWdodCcsICdub25lJyk7XHJcblx0XHRcdCRpbm5lci5jc3MoJ21heEhlaWdodCcsIG1heEhlaWdodCk7XHJcblx0XHR9XHJcblxyXG5cclxuXHRcdCRjb250YWluZXIuc2Nyb2xsVG9wKDApO1xyXG5cclxuXHJcblx0XHR2YXIgJGJvZHkgPSAkKCdib2R5Jyk7XHJcblxyXG5cdFx0dmFyICRiYXJzID0ge307XHJcblx0XHQkLmVhY2goZGlycywgaW5pdEJhcik7XHJcblxyXG5cdFx0JGlubmVyLm9uKCdzY3JvbGwnLCB1cGRhdGVCYXJzKTtcclxuXHRcdHVwZGF0ZUJhcnMoKTtcclxuXHRcdGlmIChvcHRpb25zLnByZXZlbnRQYXJlbnRTY3JvbGwpIHByZXZlbnRQYXJlbnRTY3JvbGwoKTtcclxuXHJcblx0XHR2YXIgZGF0YSA9IHtcclxuXHRcdFx0JGNvbnRhaW5lcjogJGNvbnRhaW5lcixcclxuXHRcdFx0JGJhcjogJGJhcnMueSxcclxuXHRcdFx0JGJhclg6ICRiYXJzLngsXHJcblx0XHRcdCRpbm5lcjogJGlubmVyLFxyXG5cdFx0XHRkZXN0cm95OiBkZXN0cm95LFxyXG5cdFx0XHR1cGRhdGVCYXJzOiB1cGRhdGVCYXJzLFxyXG5cdFx0XHRvcHRpb25zOiBvcHRpb25zXHJcblx0XHR9O1xyXG5cdFx0JGNvbnRhaW5lci5kYXRhKCdjdXN0b20tc2Nyb2xsJywgZGF0YSk7XHJcblx0XHRyZXR1cm4gZGF0YTtcclxuXHJcblxyXG5cdFx0ZnVuY3Rpb24gcHJldmVudFBhcmVudFNjcm9sbCgpIHtcclxuXHRcdFx0JGlubmVyLm9uKCdET01Nb3VzZVNjcm9sbCBtb3VzZXdoZWVsJywgZnVuY3Rpb24oZSkge1xyXG5cdFx0XHRcdHZhciAkdGhpcyA9ICQodGhpcyk7XHJcblx0XHRcdFx0dmFyIHNjcm9sbFRvcCA9IHRoaXMuc2Nyb2xsVG9wO1xyXG5cdFx0XHRcdHZhciBzY3JvbGxIZWlnaHQgPSB0aGlzLnNjcm9sbEhlaWdodDtcclxuXHRcdFx0XHR2YXIgaGVpZ2h0ID0gJHRoaXMuaGVpZ2h0KCk7XHJcblx0XHRcdFx0dmFyIGRlbHRhID0gKGUudHlwZSA9PSAnRE9NTW91c2VTY3JvbGwnID8gZS5vcmlnaW5hbEV2ZW50LmRldGFpbCAqIC00MCA6IGUub3JpZ2luYWxFdmVudC53aGVlbERlbHRhKTtcclxuXHRcdFx0XHR2YXIgdXAgPSBkZWx0YSA+IDA7XHJcblxyXG5cdFx0XHRcdGlmICh1cCA/IHNjcm9sbFRvcCA9PT0gMCA6IHNjcm9sbFRvcCA9PT0gc2Nyb2xsSGVpZ2h0IC0gaGVpZ2h0KSB7XHJcblx0XHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7XHJcblx0XHRcdFx0fVxyXG5cdFx0XHR9KTtcclxuXHRcdH1cclxuXHJcblx0XHRmdW5jdGlvbiBpbml0QmFyKGRpcktleSwgZGlyKSB7XHJcbi8vXHRcdFx0Y29uc29sZS5sb2coJ2luaXRCYXInLCBkaXJLZXksIGRpcilcclxuLy9cdFx0XHR2YXIgZGlyID0gRElSU1tkaXJLZXldO1xyXG5cdFx0XHQkY29udGFpbmVyWydzY3JvbGwnICsgZGlyLkRpcl0oMCk7XHJcblxyXG5cdFx0XHR2YXIgY2xzID0gb3B0aW9ucy5wcmVmaXgrJ2JhcicrZGlyLnN1ZmZpeDtcclxuXHRcdFx0dmFyICRiYXIgPSAkY29udGFpbmVyLmNoaWxkcmVuKCcuJysgY2xzKTtcclxuXHRcdFx0aWYgKCEkYmFyLmxlbmd0aCkge1xyXG5cdFx0XHRcdCRiYXIgPSAkKG9wdGlvbnMuYmFySHRtbCkuYWRkQ2xhc3MoY2xzKS5hcHBlbmRUbygkY29udGFpbmVyKTtcclxuXHRcdFx0fVxyXG5cclxuXHRcdFx0JGJhci5vbignbW91c2Vkb3duIHRvdWNoc3RhcnQnLCBmdW5jdGlvbihlKSB7XHJcblx0XHRcdFx0ZS5wcmV2ZW50RGVmYXVsdCgpOyAvLyBzdG9wIHNjcm9sbGluZyBpbiBpZTlcclxuXHRcdFx0XHR2YXIgc2Nyb2xsU3RhcnQgPSAkaW5uZXJbJ3Njcm9sbCcgKyBkaXIuRGlyXSgpO1xyXG5cdFx0XHRcdHZhciBwb3NTdGFydCA9IGVbZGlyLmNsaWVudEF4aXNdIHx8IGUub3JpZ2luYWxFdmVudC5jaGFuZ2VkVG91Y2hlcyAmJiBlLm9yaWdpbmFsRXZlbnQuY2hhbmdlZFRvdWNoZXNbMF1bZGlyLmNsaWVudEF4aXNdO1xyXG5cdFx0XHRcdHZhciByYXRpbyA9IGdldERpbXMoZGlyS2V5LCBkaXIpLnJhdGlvO1xyXG5cclxuXHRcdFx0XHQkYm9keS5vbignbW91c2Vtb3ZlLmN1c3RvbS1zY3JvbGwgdG91Y2htb3ZlLmN1c3RvbS1zY3JvbGwnLCBmdW5jdGlvbihlKSB7XHJcblx0XHRcdFx0XHRlLnByZXZlbnREZWZhdWx0KCk7IC8vIHN0b3Agc2Nyb2xsaW5nXHJcblx0XHRcdFx0XHR2YXIgcG9zID0gZVtkaXIuY2xpZW50QXhpc10gfHwgZS5vcmlnaW5hbEV2ZW50LmNoYW5nZWRUb3VjaGVzICYmIGUub3JpZ2luYWxFdmVudC5jaGFuZ2VkVG91Y2hlc1swXVtkaXIuY2xpZW50QXhpc107XHJcblx0XHRcdFx0XHQkaW5uZXJbJ3Njcm9sbCcgKyBkaXIuRGlyXShzY3JvbGxTdGFydCArIChwb3MtcG9zU3RhcnQpL3JhdGlvKTtcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0XHQkYm9keS5vbignbW91c2V1cC5jdXN0b20tc2Nyb2xsIHRvdWNoZW5kLmN1c3RvbS1zY3JvbGwnLCBmdW5jdGlvbigpIHtcclxuXHRcdFx0XHRcdCRib2R5Lm9mZignLmN1c3RvbS1zY3JvbGwnKTtcclxuXHRcdFx0XHR9KTtcclxuXHRcdFx0fSk7XHJcblx0XHRcdCRiYXJzW2RpcktleV0gPSAkYmFyO1xyXG5cdFx0fVxyXG5cclxuXHRcdGZ1bmN0aW9uIGdldERpbXMoZGlyS2V5LCBkaXIpIHtcclxuLy9cdFx0XHRjb25zb2xlLmxvZygnZ2V0RGltcycsIGRpcktleSwgZGlyKVxyXG5cdFx0XHR2YXIgdG90YWwgPSAkaW5uZXIucHJvcCgnc2Nyb2xsJyArIGRpci5EaW0pfDA7XHJcblx0XHRcdHZhciBkaW0gPSAkY29udGFpbmVyWydpbm5lcicgKyBkaXIuRGltXSgpO1xyXG5cdFx0XHR2YXIgaW5uZXIgPSAkaW5uZXJbJ2lubmVyJyArIGRpci5EaW1dKCk7XHJcblx0XHRcdHZhciBzY3JvbGwgPSBkaW0gLSBvcHRpb25zWydvZmZzZXQnICsgZGlyLkRpcl0gLSBvcHRpb25zWydvZmZzZXQnICsgZGlyLkRpcjJdO1xyXG5cdFx0XHRpZiAoIWlzQmFySGlkZGVuW2RpcktleSA9PSAneCcgPyAneScgOiAneCddKSBzY3JvbGwgLT0gb3B0aW9uc1sndHJhY2snK2Rpci5EaW1dO1xyXG5cclxuXHRcdFx0dmFyIGJhciA9IE1hdGgubWF4KChzY3JvbGwqZGltL3RvdGFsKXwwLCBvcHRpb25zWydiYXJNaW4nICsgZGlyLkRpbV0pO1xyXG5cdFx0XHR2YXIgcmF0aW8gPSAoc2Nyb2xsLWJhcikvKHRvdGFsLWlubmVyKTtcclxuLy9cdFx0XHRpZiAoZGlyS2V5ID09ICd5JyAmJiAkY29udGFpbmVyLmlzKCcjZXhhbXBsZS1oYXJkJykpIGNvbnNvbGUubG9nKCdkaW0nLCBkaW0sIGlubmVyLCBzY3JvbGwsIHRvdGFsLCBiYXIsIHJhdGlvKVxyXG5cclxuXHRcdFx0cmV0dXJuIHtcclxuXHRcdFx0XHRyYXRpbzogcmF0aW8sXHJcblx0XHRcdFx0ZGltOiBkaW0sXHJcblx0XHRcdFx0c2Nyb2xsOiBzY3JvbGwsXHJcblx0XHRcdFx0dG90YWw6IHRvdGFsLFxyXG5cdFx0XHRcdGJhcjogYmFyXHJcblx0XHRcdH1cclxuXHRcdH1cclxuXHJcblx0XHRmdW5jdGlvbiB1cGRhdGVCYXJzKCkge1xyXG5cdFx0XHQkLmVhY2goZGlycywgdXBkYXRlQmFyKTtcclxuXHRcdH1cclxuXHRcdGZ1bmN0aW9uIHVwZGF0ZUJhcihkaXJLZXksIGRpcikge1xyXG4vL1x0XHRcdHZhciBkaXIgPSBESVJTW2RpcktleV07XHJcblx0XHRcdHZhciBkaW1zID0gZ2V0RGltcyhkaXJLZXksIGRpcik7XHJcblx0XHRcdGlmICghZGltcy50b3RhbCkgcmV0dXJuO1xyXG5cclxuXHRcdFx0dmFyIHNjcm9sbFBvcyA9ICRpbm5lclsnc2Nyb2xsJyArIGRpci5EaXJdKCk7XHJcblx0XHRcdGlmIChcclxuXHRcdFx0XHRsYXN0RGltc1tkaXJLZXldLnNjcm9sbFBvcyA9PT0gc2Nyb2xsUG9zICYmXHJcblx0XHRcdFx0bGFzdERpbXNbZGlyS2V5XS5zY3JvbGwgPT09IGRpbXMuc2Nyb2xsICYmXHJcblx0XHRcdFx0bGFzdERpbXNbZGlyS2V5XS50b3RhbCA9PT0gZGltcy50b3RhbFxyXG5cdFx0XHQpIHJldHVybjtcclxuXHRcdFx0bGFzdERpbXNbZGlyS2V5XSA9IGRpbXM7XHJcblx0XHRcdGxhc3REaW1zW2RpcktleV0uc2Nyb2xsUG9zID0gc2Nyb2xsUG9zO1xyXG5cclxuXHJcblx0XHRcdHZhciBpc0hpZGUgPSBkaW1zLmJhcj49ZGltcy5zY3JvbGw7XHJcblx0XHRcdGlmIChpc0hpZGUhPT1pc0JhckhpZGRlbltkaXJLZXldKSB7XHJcblx0XHRcdFx0JGNvbnRhaW5lci50b2dnbGVDbGFzcyhvcHRpb25zLnByZWZpeCsnaGlkZGVuJytkaXIuc3VmZml4LCBpc0hpZGUpO1xyXG5cdFx0XHRcdGlzQmFySGlkZGVuW2RpcktleV0gPSBpc0hpZGU7XHJcblx0XHRcdH1cclxuXHRcdFx0dmFyIGJhclBvcyA9IHNjcm9sbFBvcypkaW1zLnJhdGlvO1xyXG4vL1x0XHRcdGNvbnNvbGUubG9nKCd1cGQnLCBzY3JvbGxQb3MsIGRpbXMucmF0aW8sIGJhclBvcylcclxuXHRcdFx0Ly9pZiAoZGlyS2V5ID09PSAneScpIGNvbnNvbGUubG9nKGJhclBvcywgZGltcy5zY3JvbGwsIGRpbXMuYmFyLCBkaW1zKVxyXG5cdFx0XHRpZiAoYmFyUG9zPDApIGJhclBvcyA9IDA7XHJcblx0XHRcdGlmIChiYXJQb3M+ZGltcy5zY3JvbGwtZGltcy5iYXIpIGJhclBvcyA9IGRpbXMuc2Nyb2xsLWRpbXMuYmFyO1xyXG5cdFx0XHQkYmFyc1tkaXJLZXldW2Rpci5kaW1dKGRpbXMuYmFyKVxyXG5cdFx0XHRcdC5jc3MoZGlyLmRpciwgb3B0aW9uc1snb2Zmc2V0JyArIGRpci5EaXJdICsgYmFyUG9zKTtcclxuXHRcdH1cclxuXHJcblx0XHRmdW5jdGlvbiBkZXN0cm95KCkge1xyXG5cdFx0XHQkLmVhY2goZGlycywgZnVuY3Rpb24oa2V5KSB7ICRiYXJzW2tleV0ucmVtb3ZlKCk7IH0pO1xyXG5cdFx0XHQkY29udGFpbmVyXHJcblx0XHRcdFx0LnJlbW92ZUNsYXNzKG9wdGlvbnMucHJlZml4Kydjb250YWluZXInKVxyXG5cdFx0XHRcdC5yZW1vdmVEYXRhKCdjdXN0b20tc2Nyb2xsJylcclxuXHRcdFx0XHQuY3NzKHtwYWRkaW5nOiAnJywgbWF4SGVpZ2h0OiAnJ30pO1xyXG5cdFx0XHQkaW5uZXIuY29udGVudHMoKS5hcHBlbmRUbygkY29udGFpbmVyKTtcclxuXHRcdFx0JGlubmVyLnJlbW92ZSgpO1xyXG5cdFx0fVxyXG5cdH1cclxufSkoalF1ZXJ5KTsiXSwiZmlsZSI6ImpxdWVyeS5jdXN0b20tc2Nyb2xsLmpzIiwic291cmNlUm9vdCI6Ii9zb3VyY2UvIn0=
