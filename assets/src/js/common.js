var menu = (function(module){

	module.menuWrap = document.getElementById('headMenu');
	module.menuBtn = document.getElementById('headMobileBtn');

	module.init = function(){
		module.menuBtn.addEventListener('click', function(){
			module.menuWrap.classList.toggle('head__menu--open');
		})
	}

	module.init();

	return module;
})(menu || {});