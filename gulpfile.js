'use strict';

var gulp        = require('gulp'),
    prefixer    = require('gulp-autoprefixer'), 
    uglify      = require('gulp-uglify'),     
    rigger      = require('gulp-rigger'),     
    sass        = require('gulp-sass'),
    notify      = require('gulp-notify'),
    sourcemaps  = require('gulp-sourcemaps'),
    cssmin      = require('gulp-clean-css'),  
    rename      = require('gulp-rename'),
    imagemin    = require('gulp-imagemin'), 
    pngquant    = require('imagemin-pngquant'), 
    spritesmith = require('gulp.spritesmith'),
    plumber     = require('gulp-plumber'),
    concat      = require('gulp-concat');

var path = {
    build: {
        js:            'assets/build/js/',
        jsPath:        'assets/build/js/',
        css:           'assets/build/css/',
        cssPath:       'assets/build/css/',
        images:        'assets/build/images/',
    },
    src: {
        // js:            'src/js/script.js',
        js:            'assets/src/js/**/*.js',
        styles:        'assets/src/scss/**/*.scss*',
        images:        'assets/src/images/**/*.*',
    },
    watch: {
        js:    'assets/src/js/**/*.js',
        styles:'assets/src/scss/**/*.scss',
        images:'assets/src/images/**/*.*',
    }
};

var supportingBrowsers = [
  '> 3%',
  'last 2 versions',
  'ie 9',
  'ie 10'
];

gulp.task('js:build', function () {
    gulp.src(path.src.js)               // Найдем наш main файл
        .pipe(plumber())
        .pipe(rigger())                 // Прогоним через rigger
        .pipe(sourcemaps.init())        // Инициализируем sourcemap
        .pipe(sourcemaps.write())       // Пропишем карты
        .pipe(gulp.dest(path.build.js)) // Выплюнем готовый файл в build
        .pipe(rename({suffix: '.min'}))
        .pipe(uglify())                 // Сожмем наш js
        .pipe(gulp.dest(path.build.js))
        .pipe(plumber.stop())
        .pipe(notify("js create!"));
});

gulp.task('styles:build', function () {
    gulp.src(path.src.styles)            // Выберем наш main.scss
        .pipe(plumber())
        .pipe(sourcemaps.init())         // То же самое что и с js
        .pipe(sass())                    // Скомпилируем
        .pipe(prefixer(supportingBrowsers)) // Добавим вендорные префиксы
        .pipe(sourcemaps.write())        // Пропишем карты
        .pipe(gulp.dest(path.build.css))               
        .pipe(rename({suffix: '.min'}))
        .pipe(cssmin())                  // Сожмем
        .pipe(gulp.dest(path.build.css)) // И в build
        .pipe(plumber.stop())
        .pipe(notify("sass create!"));
});

gulp.task('image:build', function () {
    gulp.src(path.src.images) //Выберем наши картинки
        .pipe(plumber())
        .pipe(imagemin({   //Сожмем их
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()],
            interlaced: true
        }))
        .pipe(plumber.stop())
        .pipe(gulp.dest(path.build.images))
        .pipe(notify("images create!"));
});

gulp.task('build', [
    'js:build',
    'styles:build',
    'image:build',
]);

gulp.task('watch', function(){
    gulp.watch([path.watch.js], ['js:build']);
    gulp.watch([path.watch.styles], ['styles:build']);
    gulp.watch([path.watch.images], ['image:build']);
});

gulp.task('default', ['build', 'watch']);